
extends RigidBody2D

var Speed = 0.6;
var RotateSpeed = 0.7;
var SpeedUp = false;
var SpeedDown = false;
var RotateRight = false;
var RotateLeft = false;
var Lose = false;
var CooldownMax = 15;
var Cooldown = 0;
var MaxStrength = 1200;
var Strength = 100;
var Respawn = 3.0;
var ShootPressed = false;

func _ready():
	get_node("/root/Global").set_start_pos(get_pos());
	get_node("Bow/Arrow").set_hidden(true);
	set_fixed_process(true);

func _fixed_process(delta):
	Cooldown -= 1;
	var dir = Vector2(0, 0);
	SpeedUp = false;
	SpeedDown = false;
	RotateRight = false;
	RotateLeft = false;
	
	if (Lose == false):
		var mouse_pos = get_global_mouse_pos();
		var mouse_dir = mouse_pos - get_pos();
		mouse_dir = mouse_dir.normalized();
		var bow = get_node("Bow");
		
		if (Strength > MaxStrength / 3 && ShootPressed == true):
			bow.set_frame(2);
			get_node("Bow/Arrow").set_pos(Vector2(16, 0));
		if (Strength > (MaxStrength / 3) * 2 && ShootPressed == true):
			bow.set_frame(3);
			get_node("Bow/Arrow").set_pos(Vector2(0, 0));
		if (Input.is_action_pressed("restart")):
			get_node("JointSteamEngine1").queue_free();
			get_node("JointSteamEngine2").queue_free();
			get_node("/root/Global/Sound").set_pos(get_pos());
			get_node("/root/Global/Sound").play("Die");
			Lose = true;
		
		if (atan(mouse_dir.y/mouse_dir.x) < 0 && mouse_dir.y <= 0):
			bow.set_rot(-atan(mouse_dir.y/mouse_dir.x) - get_rot());
		elif (mouse_dir.y <= 0):
			bow.set_rot(atan(mouse_dir.x/mouse_dir.y) + (PI/2) - get_rot());
		else:
			bow.set_rot(atan(mouse_dir.x/mouse_dir.y) - (PI/2) - get_rot());
		
		if (Input.is_action_pressed("move_up")):
			SpeedUp = true;
			SpeedDown = false;
		if (Input.is_action_pressed("move_down")):
			SpeedDown = true;
			SpeedUp = false;
		if (Input.is_action_pressed("move_right")):
			RotateRight = true;
			RotateLeft = false;
		if (Input.is_action_pressed("move_left")):
			RotateRight = false;
			RotateLeft = true;
		
		if (Input.is_action_pressed("shoot") && ShootPressed == false):
			ShootPressed = true;
			Strength = 100;
			bow.set_frame(1);
			get_node("Bow/Arrow").set_hidden(false);
			get_node("Bow/Arrow").set_pos(Vector2(32, 0));
		elif (Input.is_action_pressed("shoot") && ShootPressed == true):
			Strength += 5;
			if (Strength > MaxStrength):
				Strength = MaxStrength;
		elif (ShootPressed == true):
			ShootPressed = false;
			shoot();
			bow.set_frame(0);
			get_node("Bow/Arrow").set_hidden(true);
		
	elif (Respawn > 0.0):
		Respawn -= delta;
		if (Respawn <= 0.0):
			queue_free();

func _integrate_forces(state):
	if (RotateRight == true):
		set_angular_velocity(get_angular_velocity() + RotateSpeed);
	elif (RotateLeft == true):
		set_angular_velocity(get_angular_velocity() - RotateSpeed);

func shoot():
	var arrow = preload("res://assets/scenes/Arrow.tscn").instance();
	var mouse_pos = get_global_mouse_pos();
	var mouse_dir = mouse_pos - get_pos() - Vector2(0, 16);
	mouse_dir = mouse_dir.normalized();
	var velocity = mouse_dir * Strength;
	arrow.set_pos(get_pos() - Vector2(0, 16));
	if (atan(velocity.y/velocity.x) < 0 && velocity.y <= 0):
		arrow.set_rot(-atan(velocity.y/velocity.x));
	elif (velocity.y <= 0):
		arrow.set_rot(atan(velocity.x/velocity.y) + (PI/2));
	else:
		arrow.set_rot(atan(velocity.x/velocity.y) - (PI/2));
	arrow.set_linear_velocity(velocity);
	get_parent().add_child(arrow);
	get_node("/root/Global/Sound").set_pos(get_pos());
	get_node("/root/Global/Sound").play("Shoot");