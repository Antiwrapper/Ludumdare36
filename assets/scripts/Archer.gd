
extends KinematicBody2D

var Target = Vector2();
var Death = false;
var StartShooting = false;
var CooldownMax = rand_range(3, 6);
var Cooldown = CooldownMax;
var Offset = rand_range(10, 100);

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (get_parent().has_node("Player")):
		Target = get_node("../Player").get_pos() - Vector2(0, Offset);
		var dir = Target - get_pos();
		if (sign(dir.x) ==  1):
			get_node("Sprite").set_flip_h(true);
			get_node("Sprite").set_offset(Vector2(-8, 0));
		else:
			get_node("Sprite").set_flip_h(false);
			get_node("Sprite").set_offset(Vector2(8, 0));
		if (dir.length() < 640):
			if (StartShooting == false):
				StartShooting = true;
			elif (Cooldown > 0):
				Cooldown -= delta;
			else:
				get_node("Bow").set_frame(0);
				get_node("Bow/Arrow").set_hidden(true);
				shoot(dir);
				Offset = rand_range(10, 100);
				StartShooting = false;
				Cooldown = CooldownMax;
			var bow = get_node("Bow");
			var arrow = get_node("Bow/Arrow");
			if (Cooldown < (CooldownMax / 2.5) * 2):
				arrow.set_hidden(false);
				bow.set_frame(1);
				arrow.set_offset(Vector2(28, 0));
			if (Cooldown < (CooldownMax / 3) * 2):
				bow.set_frame(2);
				arrow.set_offset(Vector2(16, 0));
			if (Cooldown < (CooldownMax / 3)):
				bow.set_frame(3);
				arrow.set_offset(Vector2(0, 0));
			
			if (atan(dir.y/dir.x) < 0 && dir.y <= 0):
				bow.set_rot(-atan(dir.y/dir.x) - get_rot());
			elif (dir.y <= 0):
				bow.set_rot(atan(dir.x/dir.y) + (PI/2) - get_rot());
			else:
				bow.set_rot(atan(dir.x/dir.y) - (PI/2) - get_rot());
	if (Death == true):
		queue_free();

func shoot(Dir):
	Dir = Dir.normalized();
	var arrow = preload("res://assets/scenes/ArrowEnemy.tscn").instance();
	arrow.set_pos(get_pos());
	arrow.set_rot(get_node("Bow").get_rot());
	arrow.set_linear_velocity(Dir * rand_range(500, 800));
	get_parent().add_child(arrow);