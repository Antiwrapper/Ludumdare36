
extends Button

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	var g = get_node("/root/Global");
	if (round(g.Time[0]) != 1000):
		get_node("../Level1Button").set_hidden(false);
	if (round(g.Time[1]) != 1000):
		get_node("../Level2Button").set_hidden(false);
	if (round(g.Time[2]) != 1000):
		get_node("../Level3Button").set_hidden(false);
	if (round(g.Time[3]) != 1000):
		get_node("../Level4Button").set_hidden(false);
		set_hidden(true);
		get_node("../ThankYou").set_hidden(false);

func _on_Button_pressed():
	var g = get_node("/root/Global");
	if (round(g.Time[0]) == 1000):
		g.Level = 1;
		g.goto_scene("res://assets/scenes/TestLevel.tscn");
	elif (round(g.Time[1]) == 1000):
		g.Level = 2;
		g.goto_scene("res://assets/scenes/Level2.tscn");
	elif (round(g.Time[2]) == 1000):
		g.Level = 3;
		g.goto_scene("res://assets/scenes/Level3.tscn");
	elif (round(g.Time[3]) == 1000):
		g.Level = 4;
		g.goto_scene("res://assets/scenes/Level4.tscn");
