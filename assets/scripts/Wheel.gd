
extends RigidBody2D

func _ready():
	pass

func _integrate_forces(state):
	if (get_parent().SpeedUp == true):
		set_angular_velocity(get_angular_velocity() + get_parent().Speed);
	elif (get_parent().SpeedDown == true):
		set_angular_velocity(get_angular_velocity() - get_parent().Speed);