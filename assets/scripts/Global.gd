
extends Node

var CurrentScene = null;
var Level = 1;
var StartPos = Vector2(0, 0);
var Time = [999.9, 999.9, 999.9, 999.9];
var MusicStreamer = StreamPlayer.new();
var SoundPlayer = SamplePlayer2D.new();

func _ready():
	var root = get_tree().get_root();
	CurrentScene = root.get_child(root.get_child_count() - 1);
	var stream = preload("res://assets/music/music.ogg");
	MusicStreamer.set_stream(stream);
	MusicStreamer.set_autoplay(true);
	MusicStreamer.set_loop(true);
	MusicStreamer.set_name("Music");
	add_child(MusicStreamer);
	MusicStreamer.play();
	
	var library = preload("res://assets/sounds/Sample.tres");
	SoundPlayer.set_sample_library(library);
	SoundPlayer.set_polyphony(8);
	SoundPlayer.set_name("Sound");
	add_child(SoundPlayer);

func set_start_pos(pos):
	StartPos = pos;

func goto_next_level():
	if (Level < 5 && get_node("/root/Root").has_node("Camera")):
		if (Time[Level - 1] > get_node("/root/Root/Camera/GUI").Time):
			Time[Level - 1] = get_node("/root/Root/Camera/GUI").Time;
	goto_scene("res://assets/scenes/ScoreScreen.tscn");

func goto_scene(path):
	call_deferred("deferred_goto_scene", path);

func deferred_goto_scene(path):
	CurrentScene.free();
	var s = ResourceLoader.load(path);
	CurrentScene = s.instance();
	get_tree().get_root().add_child(CurrentScene);
	get_tree().set_current_scene(CurrentScene);