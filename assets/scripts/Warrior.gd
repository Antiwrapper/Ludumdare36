
extends KinematicBody2D

var Target = Vector2();
var UpdateTime = 1.5;
var Update = 0.5;
var Dir = Vector2(0, 0);
var Death = false;
var DeathTimer = 4;
var Gravity = Vector2(0, 300);

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (DeathTimer == 0):
		queue_free();
	Update -= delta;
	if (Update <= 0):
		if (get_parent().has_node("Player") == true):
			Target = get_parent().get_node("Player").get_pos() + Vector2(0, 0);
			Dir = Target - get_pos();
			if (Dir.length() < 640):
				Dir = Dir.normalized();
				if (get_parent().get_node("Player").Lose == true):
					if (Dir.x >= 0.0):
						Dir = Vector2(1, 0);
					else:
						Dir = Vector2(-1, 0)
			else:
				Dir = Vector2(0, 0);
		Update = UpdateTime;
	if (Dir.length() < 0.1):
		get_node("Sprite").set_rot(0);
	else:
		if (Dir.x < 0 && get_node("Sprite").get_rot() < 0.5):
			get_node("Sprite").set_rot(get_node("Sprite").get_rot() + deg2rad(2));
		elif (Dir.x > 0 && get_node("Sprite").get_rot() > -0.5):
			get_node("Sprite").set_rot(get_node("Sprite").get_rot() - deg2rad(2));
	if (Dir.length() < 150 && Dir.length() > 2):
		Gravity = Vector2(0, 10);
	else:
		Gravity = Vector2(0, 300);
	var motion = move(Dir * 300 * delta + (Gravity * delta));
	if (is_colliding()):
		var a = get_collision_mask();
		if (a == 2):
			queue_free();
		var n = get_collision_normal();
		motion = n.slide(motion);
		move(motion);
	if (Death == true):
		DeathTimer -= 1;
		if (DeathTimer == 1 || DeathTimer == 2 || DeathTimer == 3):
			Death = false;