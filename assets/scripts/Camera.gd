
extends Camera2D

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (Input.is_action_pressed("ui_cancel")):
		get_node("/root/Global").goto_scene("res://assets/scenes/Menu.tscn");
	if (get_parent().has_node("Player")):
		set_pos(get_parent().get_node("Player").get_pos() - (get_viewport_rect().size / 2));
	else:
		var player = preload("res://assets/scenes/Player.scn").instance();
		player.set_pos(get_node("/root/Global").StartPos);
		player.set_name("Player");
		get_parent().add_child(player);