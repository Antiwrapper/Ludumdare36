
extends KinematicBody2D

var Target = Vector2();
var UpdateTime = 1.5;
var Update = 0.5;
var Dir = Vector2(0, 0);
var Death = false;
var DeathTimer = 4;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (DeathTimer == 0):
		queue_free();
	Update -= delta;
	if (Update <= 0):
		if (get_parent().has_node("Player") == true):
			Target = get_parent().get_node("Player").get_pos() + Vector2(rand_range(-10, 10), rand_range(-30, 30));
			Dir = Target - get_pos();
			if (Dir.length() < 640):
				Dir = Dir.normalized();
				if (get_parent().get_node("Player").Lose == true):
					if (Dir.x >= 0.0):
						Dir = Vector2(1, 0);
					else:
						Dir = Vector2(-1, 0)
			else:
				Dir = Vector2(0, 0);
		Update = UpdateTime;
	if (Dir.length() < 0.1):
		get_node("Sprite").set_frame(0);
		get_node("Sprite").set_rot(0);
	else:
		get_node("Sprite").set_frame(1);
		if (Dir.x < 0 && get_node("Sprite").get_rot() < 0.8):
			get_node("Sprite").set_rot(get_node("Sprite").get_rot() + deg2rad(2));
		elif (Dir.x > 0 && get_node("Sprite").get_rot() > -0.8):
			get_node("Sprite").set_rot(get_node("Sprite").get_rot() - deg2rad(2));
	var motion = move(Dir * 300 * delta);
	if (is_colliding()):
		var a = get_collision_mask();
		if (a == 2):
			queue_free();
		var n = get_collision_normal();
		motion = n.slide(motion);
		move(motion);
	if (Death == true):
		DeathTimer -= 1;