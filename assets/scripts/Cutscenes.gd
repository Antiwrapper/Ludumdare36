
extends Control

const String1 = "Mostly because they rejected all her crazy ideas.";
const String2 = "She therefore sketched an idea on how to end humanity.";
const String3 = "The sketch included a steam powered vehicle.";
const String4 = "Her plan was to put a powerful weapon on the vehicle.";
const String5 = "But she couldn't decide on which weapon to use.";
const String6 = "Something that has a long range and high accuracy.";
const String7 = "Though she didn't have time to invent a new one.";
const String8 = "Therefore she chose a bow, an ancient weapon.";
const String9 = "Now she just needed to calculate how long it will take to execute the plan.";
var Pause = false;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (Input.is_action_pressed("ui_accept") && Pause == false):
		Pause = true;
		var c = get_node("Text").get_text();
		if (c == String1):
			get_node("Text").set_bbcode(String2);
		elif (c == String2):
			get_node("Text").set_bbcode(String3);
		elif (c == String3):
			get_node("Text").set_bbcode(String4);
		elif (c == String4):
			get_node("Text").set_bbcode(String5);
		elif (c == String5):
			get_node("Text").set_bbcode(String6);
		elif (c == String6):
			get_node("Text").set_bbcode(String7);
		elif (c == String7):
			get_node("Text").set_bbcode(String8);
		elif (c == String8):
			get_node("Text").set_bbcode(String9);
		elif (c == String9):
			get_node("/root/Global").goto_scene("res://assets/scenes/Menu.tscn");
		else:
			get_node("Text").set_bbcode(String1);
	elif (Input.is_action_pressed("ui_accept") == false):
		Pause = false;