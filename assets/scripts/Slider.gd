
extends HSlider

func _ready():
	if (get_name() == "Music"):
		set_value(round(get_node("/root/Global/Music").get_volume() * 100));
	else:
		set_value(round(get_node("/root/Global/Sound").get_sample_library().sample_get_volume_db("Die")));




func _on_Sound_value_changed( value ):
	var s = get_node("/root/Global/Sound");
	s.get_sample_library().sample_set_volume_db("Die", value);
	s.get_sample_library().sample_set_volume_db("Hurt", value);
	s.get_sample_library().sample_set_volume_db("Shoot", value);


func _on_Music_value_changed( value ):
	var m = get_node("/root/Global/Music");
	m.set_volume(value / 100);