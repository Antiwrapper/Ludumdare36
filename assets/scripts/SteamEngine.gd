
extends RigidBody2D

func _ready():
	pass

func _integrate_forces(state):
	if (state.get_contact_count() > 0):
		var s = state.get_contact_collider_object((0));
		if (s.has_node("Star") == false):
			if (get_parent().Lose == false):
				get_node("/root/Global/Sound").set_pos(get_parent().get_pos());
				get_node("/root/Global/Sound").play("Die");
				get_parent().get_node("JointSteamEngine1").queue_free();
				get_parent().get_node("JointSteamEngine2").queue_free();
				get_parent().Lose = true;
		else:
			if (get_parent().Lose == false):
				get_node("/root/Global").goto_next_level();