
extends Button

func _ready():
	pass

func _on_Level1Button_pressed():
	get_node("/root/Global").Level = 1;
	get_node("/root/Global").goto_scene("res://assets/scenes/TestLevel.tscn");


func _on_Level2Button__pressed():
	get_node("/root/Global").Level = 2;
	get_node("/root/Global").goto_scene("res://assets/scenes/Level2.tscn");


func _on_Level3Button_pressed():
	get_node("/root/Global").Level = 3;
	get_node("/root/Global").goto_scene("res://assets/scenes/Level3.tscn");


func _on_Level4Button_pressed():
	get_node("/root/Global").Level = 4;
	get_node("/root/Global").goto_scene("res://assets/scenes/Level4.tscn");