
extends Control

var Time = 0.0;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	Time += delta;
	var display_time = stepify(Time, 0.01);
	get_node("Time").set_bbcode("Time: " + var2str(display_time));