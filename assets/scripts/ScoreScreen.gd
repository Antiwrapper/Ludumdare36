
extends Control

func _ready():
	var g = get_node("/root/Global");
	set_fixed_process(true);
	
	var t = get_node("Time");
	t.set_bbcode("Level 1: " + var2str(g.Time[0]) + "\n\nLevel 2: " + var2str(g.Time[1]) + "\n\nLevel 3: " + var2str(g.Time[2]) + "\n\nLevel 4: " + var2str(g.Time[3]));

func _fixed_process(delta):
	if (Input.is_action_pressed("ui_cancel")):
		get_node("/root/Global").goto_scene("res://assets/scenes/Menu.tscn");