
extends RigidBody2D

var Time = 20;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	Time -= delta;
	if (Time < 0.1):
		queue_free();

