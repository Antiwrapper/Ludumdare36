
extends RigidBody2D

var Time = 20;
var Killable = 2;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	Time -= delta;
	if (Time < 0.1):
		queue_free();
	if (Killable == 0 || get_linear_velocity().length() < 110):
		set_collision_mask_bit(2, false);
		set_collision_mask_bit(0, false);

func _integrate_forces(state):
	if (state.get_contact_count() > 0):
		for i in range(2):
			if (i == 1 && state.get_contact_count() > 1):
				var s = state.get_contact_collider_object(i);
				if (s != null):
					if (s.get_collision_mask_bit(19) == true && get_linear_velocity().length() > 110 && Killable > 0):
						s.Death = true;
						Killable -= 1;
						get_node("/root/Global/Sound").set_pos(get_pos());
						get_node("/root/Global/Sound").play("Hurt");
			elif (i == 0):
				var s = state.get_contact_collider_object(i);
				if (s != null):
					if (s.get_collision_mask_bit(19) == true && get_linear_velocity().length() > 110 && Killable > 0):
						s.Death = true;
						Killable -= 1;
						get_node("/root/Global/Sound").set_pos(get_pos());
						get_node("/root/Global/Sound").play("Hurt");